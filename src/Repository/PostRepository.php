<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findApi($limit = null, $page = null, $search = null, $tags = null)
    {
        $offset = ($page-1) * $limit;

        $res = $this->createQueryBuilder('p')
            ->where("p.content LIKE '%$search%'")
            ->orWhere("p.title LIKE '%$search%'")
            ->getQuery()
            ->getResult();

        if(!is_null($tags)) {
            $result = [];
            foreach ($res as $val) {
                if (array_intersect($val->getTags() ? $val->getTags() : [], $tags)) {
                    $result[] = $val;

                };
            }
        }
        else $result = $res;
        return array_slice($result, $offset, $limit);



    }
}
