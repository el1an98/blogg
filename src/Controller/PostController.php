<?php

namespace App\Controller;

use App\Entity\Post;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;

class PostController extends AbstractController
{
    /**
     * @Route("/api/post", name="post_create", methods={"POST"})
     * @SWG\Response(
     *     response=200,
     *     description="Возвращает созданный пост",
     * )
     * @SWG\Parameter(
     *     name="title",
     *     in="formData",
     *     description="Заголовок",
     *     type="string"
     * ) @SWG\Parameter(
     *     name="content",
     *     in="formData",
     *     description="СОдержание поста",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[0]",
     *     in="formData",
     *     description="Тег #1",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[1]",
     *     in="formData",
     *     description="Тег #2",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[2]",
     *     in="formData",
     *     description="Тег #3",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[3]",
     *     in="formData",
     *     description="Тег #4",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[4]",
     *     in="formData",
     *     description="Тег #5",
     *     type="string"
     * )
     */
    public function create(Request $request)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $post = new Post();
        $post->setTitle( $request->request->get('title'));
        $post->setContent( $request->request->get('content'));
        $post->setTags( $request->request->get('tags'));


        $entityManager->persist($post);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        $serializer = $this->container->get('serializer');

        return new Response($serializer->serialize($post, 'json'));
    }
    /**
     * @Route("/api/posts/{page}", name="post_list", methods={"GET"})
     * @SWG\Parameter(
     *     name="search",
     *     in="query",
     *     description="Строка поиска",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="page",
     *     in="path",
     *     description="Номер страницы",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Максимальное количество постов на одной странице",
     *     type="string"
     * )
     * @SWG\Response(
     *     response=200,

     *     description="Возвращает лист постов",
     * )
     * @SWG\Parameter(
     *     name="tags[0]",
     *     in="query",
     *     description="Тег #1",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[1]",
     *     in="query",
     *     description="Тег #2",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[2]",
     *     in="query",
     *     description="Тег #3",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[3]",
     *     in="query",
     *     description="Тег #4",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[4]",
     *     in="query",
     *     description="Тег #5",
     *     type="string"
     * )
     */
    public function index(Request $request, int $page = 1)
    {

        $tags = $request->query->get('tags');
        $search = $request->query->get('search');
        $limit = $request->query->get('limit', 10);

        $repository = $this->getDoctrine()->getRepository(Post::class);
        $posts = $repository->findApi($limit, $page, $search, $tags);
        $serializer = $this->container->get('serializer');

        return new Response($serializer->serialize($posts, 'json'));
    }
    /**
     * @Route("/api/post/{id}", name="post_show", methods={"GET"})
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="ID поста",
     *     type="string"
     * )
     * @SWG\Response(
     *     response=200,

     *     description="Возвращает пост",
     * )
     */
    public function show(Post $post)
    {
        $serializer = $this->container->get('serializer');
        return new Response($serializer->serialize($post, 'json'));
    }
    /**
     * @Route("/api/post/{id}", name="post_edit", methods={"PUT"})
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="ID поста",
     *     type="string"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Возвращает измененный пост",
     * )
     * @SWG\Parameter(
     *     name="title",
     *     in="formData",
     *     description="Заголовок",
     *     type="string"
     * ) @SWG\Parameter(
     *     name="content",
     *     in="formData",
     *     description="Контент",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[0]",
     *     in="formData",
     *     description="Тег #1",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[1]",
     *     in="formData",
     *     description="Тег #2",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[2]",
     *     in="formData",
     *     description="Тег #3",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[3]",
     *     in="formData",
     *     description="Тег #4",
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="tags[4]",
     *     in="formData",
     *     description="Тег #5",
     *     type="string"
     * )
     */
    public function edit(Post $post, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();


        if($title = $request->request->get('title'))
            $post->setTitle($title);
        if($content = $request->request->get('content'))
            $post->setContent($content);
        if($tags = $request->request->get('tags'))
            $post->setTags($tags);


        $entityManager->persist($post);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        $serializer = $this->container->get('serializer');

        return new Response($serializer->serialize($post, 'json'));
    }
    /**
     * @Route("/api/post/{id}", name="post_del", methods={"DELETE"})
     */
    public function delete(Post $post)
    {
        $id = $post->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($post);
        $entityManager->flush();
        return $this->json(['message' => "Пост #$id был удален"]);
    }
}
