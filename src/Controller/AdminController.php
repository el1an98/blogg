<?php


namespace App\Controller;


use App\Entity\Post;
use App\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/posts", name="admin_posts", methods={"GET"})
     */
    public function index()
    {
        $form = $this->createFormBuilder(null)
            ->add('search')
            ->add('page')
            ->add('limit')
            ->add('find', SubmitType::class, ['label' => 'Find'])
            ->getForm();
        return $this->render('admin/index.twig', ['form' => $form->createView()]);
    }
    /**
     * @Route("/admin/post/{id}", name="admin_edit_post", methods={"GET"})
     */
    public function form(Post $post)
    {
        $form = $this->createForm(PostType::class, $post, [
        'action' => $this->generateUrl('post_edit', ['id'=> $post->getId()]),
            'method' => 'PUT',
        ]);

        return $this->render('admin/post.twig', [ 'form' => $form->createView()]);
    }
    /**
     * @Route("/admin/post", name="admin_new_post", methods={"GET"})
     */
    public function new_form()
    {
        $form = $this->createForm(PostType::class, new Post(), [
            'action' => $this->generateUrl('post_create'),
            'method' => 'POST',
        ]);

        return $this->render('admin/post.twig', [ 'form' => $form->createView()]);
    }

}